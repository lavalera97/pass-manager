from tkinter import *
from tkinter import messagebox
from string import ascii_lowercase, ascii_uppercase, digits
import random
import pyperclip
import json


# ---------------------------- FIND PASSWORD ------------------------------------ #

def find_password():
    site = entry_site.get()
    with open('data.json') as file:
        dict = json.load(file)
    try:
        needed_site = dict[site]
        messagebox.showinfo(title=site, message=f"User details:\n\nEmail: {needed_site['email']}\nPassword: {needed_site['password']}")
        pyperclip.copy(text=f"Email: {needed_site['email']}\nPassword: {needed_site['password']}")
    except KeyError:
        messagebox.showinfo(title='Oooops', message="There is no registered account on this site")
        entry_site.delete(0, END)
    except json.decoder.JSONDecodeError:
        messagebox.showinfo(title='Error', message="There is no entries in the file")
        entry_site.delete(0, END)


# ---------------------------- PASSWORD GENERATOR ------------------------------- #

def generate_password():
    entry_password.delete(0, END)
    letters = list(ascii_uppercase + ascii_lowercase)
    numbers = list(digits)
    symbols = ['!', '#', '$', '%', '*', '+']

    nr_letters = random.randint(7, 9)
    nr_symbols = 1
    nr_numbers = random.randint(2, 3)
    password_letters = [random.choice(letters) for _ in range(nr_letters)]
    password_symbols = [random.choice(symbols) for _ in range(nr_symbols)]
    password_number = [random.choice(numbers) for _ in range(nr_numbers)]

    password_list = password_number + password_symbols + password_letters

    random.shuffle(password_list)
    password = ''.join(password_list)
    pyperclip.copy(password)
    entry_password.insert(END, string=f'{password}')


# ---------------------------- SAVE PASSWORD ------------------------------- #

def save_data():
    site = entry_site.get()
    mail = entry_mail.get()
    password = entry_password.get()
    new_data = {
        site: {
            'email': mail,
            'password': password,
        }
    }

    if len(site) == 0 or len(password) == 0 or len(mail) == 0:
        messagebox.showerror(title='Oooops', message='Type your info again')
    else:
        try:
            with open('data.json', mode='r') as file:
                data = json.load(file)

        except FileNotFoundError:
            with open('data.json', mode='w') as file:
                json.dump(new_data, file, indent=4)
        else:
            data.update(new_data)
            with open('data.json', mode='w') as file:
                json.dump(data, file, indent=4)
        finally:

            entry_site.delete(0, END)
            entry_password.delete(0, END)


# ---------------------------- UI SETUP ------------------------------- #

window = Tk()
window.title("Password Manager")
window.config(padx=40, pady=40)

canvas = Canvas(width=200, height=200)
logo = PhotoImage(file='logo.png')
canvas.create_image(100, 100, image=logo)
canvas.grid(row=0, column=1)


# labels
label1 = Label(text='Website:', font=('Arial', 10))
label1.grid(row=1, column=0)

label2 = Label(text='Email/Username:', font=('Arial', 10))
label2.grid(row=2, column=0)

label3 = Label(text='Password:', font=('Arial', 10))
label3.grid(row=3, column=0)

# entries

entry_site = Entry(width=21, justify='center', font=('Arial', 10))
entry_site.grid(row=1, column=1, pady=5)
entry_site.focus()

entry_mail = Entry(width=37, justify='center', font=('Arial', 10))
entry_mail.grid(row=2, column=1, columnspan=2, pady=5)

entry_password = Entry(width=21, justify='center', font=('Arial', 10))
entry_password.grid(row=3, column=1, pady=5)


generate_button = Button(text='Generate Password', command=generate_password)
generate_button.grid(row=3, column=2)

add_button = Button(text='Add', width=36, command=save_data)
add_button.grid(row=4, column=1, columnspan=2, pady=5)

search_button = Button(text='Search', width=12, command=find_password)
search_button.grid(row=1, column=2)


window.mainloop()
